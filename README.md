# Frida-CheatSheet

## Installation

### Install Frida on Linux

```
pip install frida-tools
```

### Auto install on device

https://github.com/AndroidTamer/frida-push

```
pip install --user frida-push
adb devices -l
frida-push -d [Name of the device]
```

## Useful scripts

### Skeleton

https://github.com/Margular/frida-skeleton

or 

```javascript
setTimeout(function(){
    Java.perform(function (){
        }
    })
```

### Launch script

```bash
frida -U [package name] -l [script path]
```